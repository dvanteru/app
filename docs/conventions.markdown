﻿#Project Conventions

1. All micro configuration files will be placed under the source/config folder with a .erb extension suffixing their real extension.

2. All tests for a component will live in a file under the app.specs folder with the name [Component]Specs.cs.

3. Variable names and method names will use all underscore naming style. No leading underscore for variables

4. All components with dependencies will be ensured valid dependencies upon creation.

5. Null is not an allowable return value for methods on components we create. (Favour null object or throwing exceptions).

6. Stub usage must be brokered through the Stub gateway.
