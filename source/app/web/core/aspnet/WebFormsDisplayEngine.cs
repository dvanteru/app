﻿namespace app.web.core.aspnet
{
  public class WebFormsDisplayEngine : IDisplayInformation
  {
    IGetTheCurrentlyExecutingRequest current_request;
    ICreateTheHttpHandlerForAReport view_factory;

    public WebFormsDisplayEngine(IGetTheCurrentlyExecutingRequest current_request,
                                 ICreateTheHttpHandlerForAReport view_factory)
    {
      this.current_request = current_request;
      this.view_factory = view_factory;
    }

    public void display<PresentationModel>(PresentationModel model)
    {
      var view = view_factory.create_view_to_display(model);
      view.ProcessRequest(current_request());
    }
  }
}