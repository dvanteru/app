﻿using System.Web;

namespace app.web.core.aspnet
{
  public class ReportHttpHandlerFactory : ICreateTheHttpHandlerForAReport
  {
    IFindThePathsToReportPages path_registry;
    ICreateRawHttpHandlers raw_handler_factory;

    public ReportHttpHandlerFactory(IFindThePathsToReportPages path_registry, ICreateRawHttpHandlers raw_handler_factory)
    {
      this.path_registry = path_registry;
      this.raw_handler_factory = raw_handler_factory;
    }

    public IHttpHandler create_view_to_display<Report>(Report report)
    {
      var view = (IDisplayA<Report>) raw_handler_factory(path_registry(typeof(Report)), typeof(IDisplayA<Report>));
      view.report = report;
      return view;
    }
  }
}