﻿namespace app.web.core
{
  public interface IRunAFeature
  {
    void process(IContainRequestDetails request);
  }
}