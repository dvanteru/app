﻿using System;
using System.Collections.Generic;
using app.web.application;
using app.web.core.aspnet;

namespace app.web.core.stubs
{
  public class StubPathRegistry
  {
    public static IFindThePathsToReportPages page_registry = (report_type) =>
    {
      var views = new Dictionary<Type, string>
      {
        {typeof(IEnumerable<DepartmentDisplayItem>), "~/views/DepartmentBrowser.aspx"},
        {typeof(IEnumerable<ProductDisplayItem>), "~/views/ProductBrowser.aspx"},
      };
      if (!views.ContainsKey(report_type))
        throw new NotImplementedException("We don't have the view for your report registered");

      return views[report_type];

    };
  }
}