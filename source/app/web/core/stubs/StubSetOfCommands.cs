﻿using System.Collections;
using System.Collections.Generic;
using app.utility.containers;
using app.web.application;
using app.web.application.stubs;

namespace app.web.core.stubs
{
  public class StubSetOfCommands : IEnumerable<IProcessOneRequest>
  {
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    public IEnumerator<IProcessOneRequest> GetEnumerator()
    {
      yield return report_command<IEnumerable<ProductDisplayItem>, GetTheProducts>();
      yield return report_command<IEnumerable<DepartmentDisplayItem>, GetTheMainDepartments>();
      yield return report_command<IEnumerable<DepartmentDisplayItem>, GetTheSubDepartments>();
    }

    IProcessOneRequest report_command<ReportModel>(IFetchInformationUsingTheRequest<ReportModel> query)
    {
      return new WebCommand(x => true, new ViewAReport<ReportModel>(
                                         Dependencies.fetch.a<IDisplayInformation>(),
                                         query
                                         ));
    }

    IProcessOneRequest report_command<ReportModel>(IFetchAReportUsingARequest<ReportModel> query)
    {
      return report_command(query.fetch_using);
    }

    IProcessOneRequest report_command<ReportModel, Query>() where Query : IFetchAReportUsingARequest<ReportModel>, new()
    {
      return report_command(new Query());
    }

    public class GetTheMainDepartments : IFetchAReportUsingARequest<IEnumerable<DepartmentDisplayItem>>
    {
      public IEnumerable<DepartmentDisplayItem> fetch_using(IContainRequestDetails request)
      {
        return new StubDepartmentRepository().get_the_main_departments();
      }
    }

    public class GetTheSubDepartments : IFetchAReportUsingARequest<IEnumerable<DepartmentDisplayItem>>
    {
      public IEnumerable<DepartmentDisplayItem> fetch_using(IContainRequestDetails request)
      {
        return new StubDepartmentRepository().get_sub_departments(request.map<DepartmentDisplayItem>());
      }
    }

    public class GetTheProducts : IFetchAReportUsingARequest<IEnumerable<ProductDisplayItem>>
    {
      public IEnumerable<ProductDisplayItem> fetch_using(IContainRequestDetails request)
      {
        return new StubDepartmentRepository().get_products_in(request.map<DepartmentDisplayItem>());
      }
    }
  }
}