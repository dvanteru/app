﻿namespace app.web.core
{
  public interface IProcessOneRequest : IRunAFeature
  {
    bool can_process(IContainRequestDetails request);
  }
}