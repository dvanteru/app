﻿namespace app.web.core
{
  public delegate IProcessOneRequest ICreateTheMissingCommand(IContainRequestDetails details);
}