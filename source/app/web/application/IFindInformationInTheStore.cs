﻿using System.Collections.Generic;

namespace app.web.application
{
  public interface IFindInformationInTheStore
  {
    IEnumerable<DepartmentDisplayItem> get_the_main_departments();
    IEnumerable<DepartmentDisplayItem> get_sub_departments(DepartmentDisplayItem department);
    IEnumerable<ProductDisplayItem> get_products_in(DepartmentDisplayItem selectedDepartment);
  }
}