﻿using System;

namespace app.web.application.stubs
{
    public static class ApplicationStubGateway
    {
        public static StubDepartmentRepository stub_department_reposity()
        {
            if (DateTime.Now < new DateTime(2013, 12, 7))
                return new StubDepartmentRepository(); 
            else
                throw new InvalidOperationException("StubDepartmentRepository has expired.");
        }
    }
}
