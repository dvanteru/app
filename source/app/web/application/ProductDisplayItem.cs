﻿namespace app.web.application
{
  public class ProductDisplayItem
  {
    public string name { get; set; }

    public decimal price { get; set; }
  }
}