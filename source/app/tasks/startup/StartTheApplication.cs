﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Compilation;
using app.utility.containers;
using app.utility.containers.basic;
using app.web.core;
using app.web.core.aspnet;
using app.web.core.stubs;

namespace app.tasks.startup
{
  public class StartTheApplication
  {
    static Dictionary<Type, ICreateOneDependency> all_factories;
    static IFetchDependencies container;

    public static void run()
    {
      all_factories = new Dictionary<Type, ICreateOneDependency>();
      var factories = new DependencyFactories(all_factories, Errors.factory_not_registered_for_type);
      container = new DependencyContainer(factories, Errors.dependency_creation_error);

      Dependencies.container_factory = () => container;

      register_dependencies();
    }

    static void register_dependencies()
    {
      register_dependency<IProcessRequests, FrontController>();
      register_dependency<IFindCommands, CommandRegistry>();
      register_dependency<IEnumerable<IProcessOneRequest>, StubSetOfCommands>();
      register_asp_net_adapters();
      register_dependency(Errors.command_not_registered_for_request);
      register_dependency<ICreateControllerRequests, StubRequestFactory>();
    }

    static void register_asp_net_adapters()
    {
      register_dependency<IDisplayInformation, WebFormsDisplayEngine>();
      register_dependency<IGetTheCurrentlyExecutingRequest>(() => HttpContext.Current);
      register_dependency<ICreateRawHttpHandlers>(
        (path, type) => (IHttpHandler) BuildManager.CreateInstanceFromVirtualPath(path, type));
      register_dependency<ICreateTheHttpHandlerForAReport, ReportHttpHandlerFactory>();
      register_dependency(StubPathRegistry.page_registry);
    }

    static void register_dependency<Contract, Implementation>() where Implementation : Contract
    {
      register_factory<Contract>(new AutomaticallyResolvingDependencyFactory(typeof(Implementation),
                                                                             ContainerFacilities.greediest_ctor_picker,
                                                                             container));
    }

    static void register_dependency<Contract>(Contract implementation)
    {
      register_factory<Contract>(new AnonymousDependencyFactory(() => implementation));
    }

    static void register_factory<Contract>(ICreateOneDependency factory)
    {
      all_factories[typeof(Contract)] = factory;
    }
  }
}