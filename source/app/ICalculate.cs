﻿namespace app
{
  public interface ICalculate
  {
    int add(int first, int second);
    void shut_off();
  }
}