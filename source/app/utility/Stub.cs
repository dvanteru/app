﻿using System;
using System.Linq;

namespace app.utility
{
  public class Stub 
  {
    public class usage
    {
      public static Func<bool> allow_until(DateTime date)
      {
        return () => DateTime.Now < date;
      }
    }
    public static StubType with<StubType>() where StubType : new ()
    {
      return with(new StubType(), usage.allow_until(new DateTime(2013,12,6,22 ,30,0))); 
    }

    public static StubType with<StubType>(params Func<bool>[] conditions) where StubType : new ()
    {
      return with(new StubType(),conditions);
    }

    public static StubType with<StubType>(StubType instance, params Func<bool>[] conditions)
    {
      if (conditions.All(x => x())) return instance;
      throw new NotImplementedException(String.Format("You can no longer use the stub of #{0}", typeof(StubType).Name));
    }
  }
}