﻿using System;

namespace app.utility.containers
{
  public class Dependencies
  {
    public static ISetupTheContainer container_factory = () =>
    {
      throw new NotImplementedException(
        "The container has not been setup, make sure you have a startup process that configures your application container correctly");
    };

    public static IFetchDependencies fetch
    {
      get { return container_factory(); }
    }
  }
}