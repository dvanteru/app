﻿using System;
using System.Collections.Generic;

namespace app.utility.containers
{
  public class DependencyFactories : IFindAFactoryForADependency
  {
    IDictionary<Type, ICreateOneDependency> factories;
    ICreateTheExceptionForAMissingDependencyFactory missing_factory_exception_builder;

    public DependencyFactories(IDictionary<Type, ICreateOneDependency> factories,
                               ICreateTheExceptionForAMissingDependencyFactory missing_factory_exception_builder)
    {
      this.factories = factories;
      this.missing_factory_exception_builder = missing_factory_exception_builder;
    }

    public ICreateOneDependency get_factory_to_create(Type dependency_to_create)
    {
      if (factories.ContainsKey(dependency_to_create)) return factories[dependency_to_create];

      throw missing_factory_exception_builder(dependency_to_create);
    }
  }
}