﻿using System;

namespace app.utility.containers.basic
{
  public class DependencyContainer : IFetchDependencies
  {
    IFindAFactoryForADependency factories;
    ICreateErrorsFromFactoryCreationErrors creation_error_factory;

    public DependencyContainer(IFindAFactoryForADependency factories,
                               ICreateErrorsFromFactoryCreationErrors creation_error_factory)
    {
      this.factories = factories;
      this.creation_error_factory = creation_error_factory;
    }

    public Dependency a<Dependency>()
    {
      return (Dependency) a(typeof(Dependency));
    }

    public object a(Type dependency_type)
    {
      try
      {
        var factory = factories.get_factory_to_create(dependency_type);
        var dependency = factory.create();
        return dependency;
      }
      catch (Exception ex)
      {
        throw creation_error_factory(dependency_type, ex);
      }
    }
  }
}