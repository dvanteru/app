﻿using System;
using System.Reflection;

namespace app.utility.containers
{
  public delegate ConstructorInfo IPickAConstructor(Type type_to_inspect);
}