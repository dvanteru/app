﻿using System;
using System.Data;
using System.Security;
using System.Security.Principal;
using System.Threading;
using Machine.Specifications;
using Rhino.Mocks;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(Calculator))]
  public class CalculatorSpecs
  {
    public abstract class concern : Observes<ICalculate, Calculator>
    {
      Establish c = () =>
      {
        connection = depends.on<IDbConnection>();
      };

      public static IDbConnection connection;
    }

    public class when_created : concern
    {
      It should_not_open_its_connection = () =>
        connection.never_received(x => x.Open());
    }

    public class when_shutting_off_the_calculator : concern
    {
      Establish c = () =>
      {
        principal = fake.an<IPrincipal>();
        spec.change(() => Thread.CurrentPrincipal).to(principal);
      };

      public class and_they_are_in_not_the_correct_security_group
      {
        Establish c = () =>
          principal.setup(x => x.IsInRole(Arg<String>.Is.Anything)).Return(false);

        Because b = () =>
          spec.catch_exception(() => sut.shut_off());

        It should_throw_a_security_exception = () =>
          spec.exception_thrown.ShouldBeAn<SecurityException>();
      }


      static IPrincipal principal;
    }

    public class when_adding : concern
    {
      public class two_positive_numbers
      {
        Establish c = () =>
        {
          command = fake.an<IDbCommand>();
          connection.setup(x => x.CreateCommand()).Return(command);
        };

        Because b = () =>
          result = sut.add(3, 1);

        It opens_a_connection_to_the_database = () =>
          connection.received(x => x.Open());

        It runs_a_command = () =>
          command.received(x => x.ExecuteNonQuery());

        It should_return_the_sum = () =>
          result.ShouldEqual(4);

        static IDbCommand command;
        static IDbDataAdapter adapter;
      }

      public class a_negative_and_a_positive
      {
        Because b = () =>
          spec.catch_exception(() => sut.add(2, -1));

        It should_throw_an_argument_exception = () =>
          spec.exception_thrown.ShouldBeAn<ArgumentException>();
      }

      static int result;
    }
  }
}