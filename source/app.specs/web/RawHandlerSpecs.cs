﻿using System.Web;
using Machine.Specifications;
using app.specs.utility;
using app.web.core;
using app.web.core.aspnet;
using developwithpassion.specifications.rhinomocks;
using developwithpassion.specifications.extensions;

namespace app.specs.web
{
  [Subject(typeof(RawHandler))]
  public class RawHandlerSpecs
  {
    public abstract class concern : Observes<IHttpHandler,
                                      RawHandler>
    {
    }

    public class when_processing_an_http_context : concern
    {
      Establish c = () =>
      {
        front_controller = depends.on<IProcessRequests>();
        request_factory = depends.on<ICreateControllerRequests>();
        a_new_controller_request = fake.an<IContainRequestDetails>();
        a_new_aspnet_request = ObjectFactory.web.create_http_context();

        request_factory.setup(x => x.create_request(a_new_aspnet_request)).Return(a_new_controller_request);
      };

      Because b = () =>
        sut.ProcessRequest(a_new_aspnet_request);

      It should_delegate_a_new_controller_request_to_the_front_controller = () =>
        front_controller.received(x => x.process(a_new_controller_request));

      static IProcessRequests front_controller;
      static IContainRequestDetails a_new_controller_request;
      static ICreateControllerRequests request_factory;
      static HttpContext a_new_aspnet_request;
    }
  }
}