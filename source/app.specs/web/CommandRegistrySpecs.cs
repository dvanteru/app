﻿ using System;
 using System.Collections.Generic;
 using System.Linq;
 using Machine.Specifications;
 using app.web.core;
 using developwithpassion.specifications.rhinomocks;
 using developwithpassion.specifications.extensions;

namespace app.specs.web
{  
  [Subject(typeof(CommandRegistry))]  
  public class CommandRegistrySpecs
  {
    public abstract class concern : Observes<IFindCommands,
                                      CommandRegistry>
    {
        
    }

   
    public class when_getting_the_command_that_can_process_a_request : concern
    {
      public class and_it_has_the_command
      {
        Establish c = () =>
        {
          all_the_commands = new List<IProcessOneRequest>();
          request = fake.an<IContainRequestDetails>();
          the_command_that_can_process_the_request = fake.an<IProcessOneRequest>();

          Enumerable.Range(1, 1000).each(x => all_the_commands.Add(fake.an<IProcessOneRequest>()));
          all_the_commands.Add(the_command_that_can_process_the_request);

          the_command_that_can_process_the_request.setup(x => x.can_process(request)).Return(true);
          depends.on<IEnumerable<IProcessOneRequest>>(all_the_commands);
        };

        Because b = () =>
          result = sut.get_the_command_that_can_process(request);

        It should_return_the_command_that_can_run_the_request = () =>
          result.ShouldEqual(the_command_that_can_process_the_request);

        static IProcessOneRequest result;
        static IProcessOneRequest the_command_that_can_process_the_request;
        static IContainRequestDetails request;
        static List<IProcessOneRequest> all_the_commands;
      }
        
      public class and_it_does_not_have_the_command
      {
        Establish c = () =>
        {
          the_exception = new Exception();
          depends.on<ICreateTheMissingCommand>(x =>
          {
            x.ShouldEqual(request);
            throw the_exception;
          });
          all_the_commands = new List<IProcessOneRequest>();
          request = fake.an<IContainRequestDetails>();

          Enumerable.Range(1, 1000).each(x => all_the_commands.Add(fake.an<IProcessOneRequest>()));

          depends.on<IEnumerable<IProcessOneRequest>>(all_the_commands);
        };

        Because b = () =>
          spec.catch_exception(() => sut.get_the_command_that_can_process(request));

        It should_throw_the_exception_using_the_missing_command_handler = () =>
          spec.exception_thrown.ShouldEqual(the_exception);

        static IProcessOneRequest result;
        static IContainRequestDetails request;
        static List<IProcessOneRequest> all_the_commands;
        static Exception the_exception;
      }
    }
  }
}
