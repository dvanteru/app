﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;
using Machine.Specifications;
using app.specs.utility;
using app.utility.containers;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs.web.application
{
  [Subject(typeof(AutomaticallyResolvingDependencyFactory))]
  public class AutomaticallyResolvingDependencyFactorySpecs
  {
    public abstract class concern : Observes<ICreateOneDependency,
                                      AutomaticallyResolvingDependencyFactory>
    {
    }

    public interface IConfigureDetailsAboutAnEndpoint<T>
    {
      void compress_all_calls();
      void encrypt_all_calls();
      void configure_call(Expression<Func<T, object>> func);
      void configure_call(Expression<Func<T, object>> func, Action<IConfigureServiceProxyOptions> options);
    }

    public class ServiceConfiguration
    {
      public void run()
      {
        ServiceFacade.configure_endpoint<IProvideData>(config =>
        {
          config.compress_all_calls();
          config.encrypt_all_calls();
          config.configure_call(x => x.get_data(), call_options =>
          {
            call_options.only_allow_roles("Admin"); 
            call_options.instrument_with<TimingWrapper>(); 
          });
          config.configure_call(x => x.get_customer_names(), call_options =>
          {
            call_options.only_allow_roles("Admin");
            call_options.allowable_on(() => true);
          });

        });
      }
    }

    public interface IConfigureServiceProxyOptions
    {
      void instrument_with<T>();
      void allowable_on(Func<bool> specification);
      void only_allow_roles(string admin);
    }
    public interface IProvideData
    {
      List<int> get_data();
      List<int> get_data(string customerName);
      List<int> get_customer_names();
      List<int> get_other_customer_names();
    }

    public interface IEnrichAnEndpoint<Target>
    {
    }

    public class EndpointConfiguration<T> : IEnrichAnEndpoint<T>
    {
    }

    public class ServiceFacade
    {
      public static void configure_endpoint<Target>(Action<IConfigureDetailsAboutAnEndpoint<Target>> configuration)
      {
      }
    }

    public class when_creating_a_dependency : concern
    {
      Establish c = () =>
      {
        connection = fake.an<IDbConnection>();
        reader = fake.an<IDataReader>();

        greediest_ctor = ObjectFactory.expressions.to_target<TypeWithLotsOfDependencies>()
                                      .ctor_pointed_at_by(() => new TypeWithLotsOfDependencies(null, null));

        depends.on(typeof(TypeWithLotsOfDependencies));
        depends.on<IPickAConstructor>(x =>
        {
          x.ShouldEqual(typeof(TypeWithLotsOfDependencies));
          return greediest_ctor;
        });
        the_container = depends.on<IFetchDependencies>();

        the_container.setup(x => x.a(typeof(IDbConnection))).Return(connection);
        the_container.setup(x => x.a(typeof(IDataReader))).Return(reader);
      };

      Because b = () =>
        result = sut.create();

      It should_create_the_dependency_with_all_of_its_dependencies_resolved_correctly = () =>
      {
        var actual_result = result.ShouldBeAn<TypeWithLotsOfDependencies>();
        actual_result.connection.ShouldEqual(connection);
        actual_result.reader.ShouldEqual(reader);
      };

      static object result;
      static IDbConnection connection;
      static IDataReader reader;
      static ConstructorInfo greediest_ctor;
      static IFetchDependencies the_container;
    }
  }

  public class TimingWrapper
  {
  }

  public class TypeWithLotsOfDependencies
  {
    public IDbConnection connection;
    public IDataReader reader;

    public TypeWithLotsOfDependencies(IDbConnection connection, IDataReader reader)
    {
      this.connection = connection;
      this.reader = reader;
    }

    public TypeWithLotsOfDependencies(IDbConnection connection)
    {
      this.connection = connection;
    }

    public TypeWithLotsOfDependencies(IDataReader reader)
    {
      this.reader = reader;
    }
  }
}