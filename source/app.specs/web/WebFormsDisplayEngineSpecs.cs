﻿using System.Web;
using Machine.Specifications;
using app.specs.utility;
using app.web.core;
using app.web.core.aspnet;
using developwithpassion.specifications.rhinomocks;
using developwithpassion.specifications.extensions;

namespace app.specs.web
{
  [Subject(typeof(WebFormsDisplayEngine))]
  public class WebFormsDisplayEngineSpecs
  {
    public abstract class concern : Observes<IDisplayInformation,
                                      WebFormsDisplayEngine>
    {
    }

    public class when_displaying_a_report : concern
    {
      Establish c = () =>
      {
        the_currently_executing_request = ObjectFactory.web.create_http_context();
        view = fake.an<IHttpHandler>();

        view_registry = depends.on<ICreateTheHttpHandlerForAReport>();
        depends.on<IGetTheCurrentlyExecutingRequest>(() => the_currently_executing_request);

        report_to_display = new MyReport();

        view_registry.setup(x => x.create_view_to_display(report_to_display)).Return(view);
      };

      Because b = () =>
        sut.display(report_to_display);

      It should_render_the_report_using_the_view_for_the_report = () =>
        view.received(x => x.ProcessRequest(the_currently_executing_request));

      static MyReport report_to_display;
      static ICreateTheHttpHandlerForAReport view_registry;
      static HttpContext the_currently_executing_request;
      static IHttpHandler view;
    }
  }

  public class MyReport
  {
  }
}