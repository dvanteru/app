﻿using System;
using System.Data;
using Machine.Specifications;
using app.utility.containers;
using app.utility.containers.basic;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(DependencyContainer))]
  public class DependencyContainerSpecs
  {
    public abstract class concern : Observes<IFetchDependencies,
                                      DependencyContainer>
    {
    }

    public class when_fetching_a_dependency : concern
    {
      public class and_it_has_the_factory_that_can_create_that_dependency
      {
        Establish c = () =>
        {
          factories = depends.on<IFindAFactoryForADependency>();
          a_connection = fake.an<IDbConnection>();
          the_factory_that_can_create_the_dependency = fake.an<ICreateOneDependency>();

          the_factory_that_can_create_the_dependency.setup(x => x.create()).Return(a_connection);

          factories.setup(x => x.get_factory_to_create(typeof(IDbConnection))).Return(the_factory_that_can_create_the_dependency);
        };

        Because b = () =>
          result = sut.a<IDbConnection>();

        It should_return_the_dependency_created_by_the_factory_for_that_dependency = () =>
          result.ShouldEqual(a_connection);

        static IDbConnection result;
        static IDbConnection a_connection;
        static IFindAFactoryForADependency factories;
        static ICreateOneDependency the_factory_that_can_create_the_dependency;

      }

      public class and_the_factory_that_creates_the_dependency_throws_an_exception
      {
        Establish c = () =>
        {
          factories = depends.on<IFindAFactoryForADependency>();
          a_third_party_exception = new Exception();
          wrapped_exception = new Exception();

          depends.on<ICreateErrorsFromFactoryCreationErrors>((type, original_exception) =>
          {
            type.ShouldEqual(typeof(IDbConnection));
            original_exception.ShouldEqual(a_third_party_exception);
            return wrapped_exception;
          });

          the_factory_that_can_create_the_dependency = fake.an<ICreateOneDependency>();
          the_factory_that_can_create_the_dependency.setup(x => x.create()).Throw(a_third_party_exception);
          factories.setup(x => x.get_factory_to_create(typeof(IDbConnection))).Return(the_factory_that_can_create_the_dependency);
        };

        Because b = () =>
          spec.catch_exception(() => sut.a<IDbConnection>());

        It should_throw_the_exce_created_by_the_dependency_creation_er_factory = () =>
          spec.exception_thrown.ShouldEqual(wrapped_exception);

        static IFindAFactoryForADependency factories;
        static ICreateOneDependency the_factory_that_can_create_the_dependency;
        static Exception a_third_party_exception;
        static Exception wrapped_exception;
      }
    }
  }
}