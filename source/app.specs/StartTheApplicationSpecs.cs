﻿using Machine.Specifications;
using app.tasks.startup;
using app.utility.containers;
using app.web.core;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(StartTheApplication))]
  public class StartTheApplicationSpecs
  {
    public abstract class concern : Observes
    {
    }

    public class when_the_application_has_completed_its_startup : concern
    {
      Because b = () =>
        StartTheApplication.run();

      It key_dependencies_should_be_able_to_be_found_using_the_container = () =>
      {
        Dependencies.fetch.a<IProcessRequests>().ShouldNotBeNull();
      };
    }
  }
}